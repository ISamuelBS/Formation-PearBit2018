package vues;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

/**
 * 
 * @author pearbit-zero
 *
 */

public class Echo extends JFrame {

	public JPanel contentPane;
	public JTextField jtfNomSujet;
	public JButton jbtModifier;
	public JButton jbtSupprimer;
	public JPanel jpPanel;
	public JButton jbtSauvegarder;
	public JButton jbtRetourner;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Echo frame = new Echo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Echo() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 582, 220);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		jpPanel = new JPanel();
		contentPane.add(jpPanel, BorderLayout.CENTER);
		jpPanel.setLayout(null);
		
		JLabel jlbTitre = new JLabel("Enregistrer sujets");
		jlbTitre.setBounds(112, 12, 199, 15);
		jpPanel.add(jlbTitre);
		
		JLabel jlbNomSujet = new JLabel("Nom du sujet:");
		jlbNomSujet.setBounds(12, 53, 181, 15);
		jpPanel.add(jlbNomSujet);
		
		jtfNomSujet = new JTextField();
		jtfNomSujet.setBounds(141, 51, 170, 19);
		jpPanel.add(jtfNomSujet);
		jtfNomSujet.setColumns(10);
		
		jbtSauvegarder = new JButton("Sauvegarder");
		jbtSauvegarder.setBounds(329, 7, 134, 25);
		jpPanel.add(jbtSauvegarder);
		
		jbtModifier = new JButton("Modifier");
		jbtModifier.setBounds(323, 48, 140, 25);
		jpPanel.add(jbtModifier);
		
		jbtSupprimer = new JButton("Supprimer");
		jbtSupprimer.setBounds(329, 85, 134, 25);
		jpPanel.add(jbtSupprimer);
		
		jbtRetourner = new JButton("Retourner");
		jbtRetourner.setBounds(329, 122, 134, 25);
		jpPanel.add(jbtRetourner);
	}
}
