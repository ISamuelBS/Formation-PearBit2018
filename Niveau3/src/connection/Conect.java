package connection;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

public class Conect {

	private Connection con = null;
	private Statement phraseSQL = null;

	public void Conectar() {
		try {
			String controler = "com.mysql.jdbc.Driver";
			Class.forName(controler).newInstance();


			con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/Niveau3", "root", "");
			phraseSQL = getConexion().createStatement();
		} catch (ClassNotFoundException e) {
			System.out.println("No se pudo cargar el controlador: " + e.getMessage());
		} catch (SQLException e) {
			System.out.println("Excepción SQL: " + e.getMessage());
		} catch (InstantiationException e) {
			System.out.println("Objeto no creado. " + e.getMessage());
		} catch (IllegalAccessException e) {
			System.out.println("Acceso ilegal. " + e.getMessage());
		}
	}

	public void fermer() {
		try {
			if (getSentenciaSQL() != null)
				getSentenciaSQL().close();

			if (getConexion() != null) {
				getConexion().close();
			}
		} catch (SQLException ignorada) {
		}
	}

	public Connection getConexion() {
		return con;
	}

	public Statement getSentenciaSQL() {
		return phraseSQL;
	}

	public static void main(String[] args) {
		Conect con = new Conect();
		con.Conectar();
	}
}