package Vue;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JRadioButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextPane;
import javax.swing.JTextArea;

public class Alpha extends JFrame {

	public JPanel contentPane;
	public JTextField jtfPrenom;
	public JTextField jtfNomPaternel;
	public JTextField jtfNomMaternel;
	public JTextField jtfAge;
	public JPanel jpPanel;
	public JRadioButton jrbProffesseur;
	public JRadioButton jrbEtudiant;
	public JTextField jtfTexte1;
	public JTextField jtfTexte2;
	public JTextField jtfTexte3;
	public JLabel lblTexte1;
	public JLabel lblTexte2;
	public JLabel lblTexte3;
	public JButton jbtEnregistrer;
	public JComboBox jcbOptions;
	public JTextArea jtaInformation;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Alpha frame = new Alpha();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Alpha() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		jpPanel = new JPanel();
		contentPane.add(jpPanel, BorderLayout.CENTER);
		jpPanel.setLayout(null);
		
		jrbProffesseur = new JRadioButton("Proffesseur", false);
		jrbProffesseur.setBounds(258, 36, 149, 23);
		jpPanel.add(jrbProffesseur);
		
		jrbEtudiant = new JRadioButton("Étudiant", false);
		jrbEtudiant.setBounds(258, 63, 149, 23);
		jpPanel.add(jrbEtudiant);
		
		JLabel lblPrenom = new JLabel("Prenom:");
		lblPrenom.setBounds(12, 40, 70, 15);
		jpPanel.add(lblPrenom);
		
		JLabel lblNomPaternel = new JLabel("Nom paternel:");
		lblNomPaternel.setBounds(12, 67, 123, 15);
		jpPanel.add(lblNomPaternel);
		
		JLabel lblNomMaternel = new JLabel("Nom maternel:");
		lblNomMaternel.setBounds(12, 90, 123, 15);
		jpPanel.add(lblNomMaternel);
		
		JLabel lblAge = new JLabel("Age:");
		lblAge.setBounds(12, 119, 70, 15);
		jpPanel.add(lblAge);
		
		jtfPrenom = new JTextField();
		jtfPrenom.setBounds(136, 38, 114, 19);
		jpPanel.add(jtfPrenom);
		jtfPrenom.setColumns(10);
		
		jtfNomPaternel = new JTextField();
		jtfNomPaternel.setBounds(136, 65, 114, 19);
		jpPanel.add(jtfNomPaternel);
		jtfNomPaternel.setColumns(10);
		
		jtfNomMaternel = new JTextField();
		jtfNomMaternel.setBounds(136, 88, 114, 19);
		jpPanel.add(jtfNomMaternel);
		jtfNomMaternel.setColumns(10);
		
		jtfAge = new JTextField();
		jtfAge.setBounds(136, 117, 114, 19);
		jpPanel.add(jtfAge);
		jtfAge.setColumns(10);
		
		JLabel lblInstruction = new JLabel("D'abord sélectionner le type de personne à enregistrer");
		lblInstruction.setBounds(12, 12, 416, 30);
		jpPanel.add(lblInstruction);
		
	    // Group the radio buttons.
	    ButtonGroup group = new ButtonGroup();
	    group.add(jrbEtudiant);
	    group.add(jrbProffesseur);
	    
	    lblTexte1 = new JLabel("");
	    lblTexte1.setBounds(12, 154, 123, 15);
	    jpPanel.add(lblTexte1);
	    
	    lblTexte2 = new JLabel("");
	    lblTexte2.setBounds(12, 181, 123, 15);
	    jpPanel.add(lblTexte2);
	    
	    lblTexte3 = new JLabel("");
	    lblTexte3.setBounds(12, 208, 123, 15);
	    jpPanel.add(lblTexte3);
	    
	    jtfTexte1 = new JTextField();
	    jtfTexte1.setBounds(136, 148, 114, 19);
	    jpPanel.add(jtfTexte1);
	    jtfTexte1.setVisible(false);
	    jtfTexte1.setColumns(10);
	    
	    jtfTexte2 = new JTextField();
	    jtfTexte2.setBounds(136, 179, 114, 19);
	    jpPanel.add(jtfTexte2);
	    jtfTexte2.setVisible(false);
	    jtfTexte2.setColumns(10);
	    
	    jtfTexte3 = new JTextField();
	    jtfTexte3.setBounds(136, 210, 114, 19);
	    jpPanel.add(jtfTexte3);
	    jtfTexte3.setVisible(false);
	    jtfTexte3.setColumns(10);
	    
	    jbtEnregistrer = new JButton("Enregistrer");
	    jbtEnregistrer.setBounds(262, 114, 166, 25);
	    jpPanel.add(jbtEnregistrer);
	    
	    jcbOptions = new JComboBox();
	    jcbOptions.setEnabled(false);
	    jcbOptions.setModel(new DefaultComboBoxModel(new String[] {"Afficher etudiants", "Afficher proffesseures", "Afficher tous"}));
	    jcbOptions.setBounds(258, 154, 170, 24);
	    jpPanel.add(jcbOptions);
	    
	    jtaInformation = new JTextArea();
	    jtaInformation.setEditable(false);
	    jtaInformation.setBounds(12, 235, 416, 143);
	    jtaInformation.setVisible(false);
	    jpPanel.add(jtaInformation);
	}
}
