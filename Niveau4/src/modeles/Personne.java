package modeles;

import java.util.ArrayList;

public class Personne {

	private String prenom;
	private String nomPaternel;
	private String nomMaternel;
	private int age;
	private String sexe;
	private ArrayList<Telephone> telephones = new ArrayList<Telephone>();
	
	public Personne() {
		super();
	}

	public Personne(String prenom, String nomPaternel, String nomMaternel, int age, String sexe,
			ArrayList<Telephone> telephones) {
		super();
		this.prenom = prenom;
		this.nomPaternel = nomPaternel;
		this.nomMaternel = nomMaternel;
		this.age = age;
		this.sexe = sexe;
		this.telephones = telephones;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNomPaternel() {
		return nomPaternel;
	}

	public void setNomPaternel(String nomPaternel) {
		this.nomPaternel = nomPaternel;
	}

	public String getNomMaternel() {
		return nomMaternel;
	}

	public void setNomMaternel(String nomMaternel) {
		this.nomMaternel = nomMaternel;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getSexe() {
		return sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

	public ArrayList<Telephone> getTelephones() {
		return telephones;
	}

	public void setTelephones(ArrayList<Telephone> telephones) {
		this.telephones = telephones;
	}
}
