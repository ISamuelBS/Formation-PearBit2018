package vues;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

//import dataBase.GestionDB;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

/**
 * 
 * @author pearbit-zero
 *
 */

public class Charlie extends JFrame {

	public JPanel contentPane;
	public JTextField jtfNomDepartament;
	public String nombreDepartamento;
	public JButton jbtModifier;
	public JButton jbtSupprimer;
		
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Charlie frame = new Charlie();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Charlie() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 556, 220);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel jpPanel = new JPanel();
		contentPane.add(jpPanel, BorderLayout.CENTER);
		jpPanel.setLayout(null);
		
		JLabel jlbTitre = new JLabel("Enregistrer departaments");
		jlbTitre.setBounds(12, 33, 209, 15);
		jpPanel.add(jlbTitre);
		
		JLabel jlbNomDepartament = new JLabel("Nom du departament:");
		jlbNomDepartament.setBounds(12, 81, 192, 15);
		jpPanel.add(jlbNomDepartament);
		
		jtfNomDepartament = new JTextField();
		jtfNomDepartament.setBounds(211, 79, 114, 19);
		jpPanel.add(jtfNomDepartament);
		jtfNomDepartament.setColumns(10);
		
		JButton jbtSauvegarder = new JButton("Sauvegarder");
		jbtSauvegarder.setBounds(350, 23, 125, 25);
		jpPanel.add(jbtSauvegarder);
		
		jbtSupprimer = new JButton("Supprimer");
		jbtSupprimer.setBounds(350, 97, 125, 25);
		jpPanel.add(jbtSupprimer);
		
		jbtModifier = new JButton("Modifier");
		jbtModifier.setBounds(350, 60, 125, 25);
		jpPanel.add(jbtModifier);
		
		JButton jbtRetourner = new JButton("Retourner");
		jbtRetourner.setBounds(350, 134, 125, 25);
		jpPanel.add(jbtRetourner);
	}
}
