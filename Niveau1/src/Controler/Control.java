package Controler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import Model.Model;
import View.Alpha;
import View.Bravo;

/**
 * Niveu 1
 * 
 * @author pearbit-zero - Iván Samuel Burgueño Sánchez
 * @version 1.0
 */

public class Control implements ActionListener, ItemListener, KeyListener {

	String arrangement[][] = new String[3][6];

	Model model = new Model();
	Alpha alpha = new Alpha();
	Bravo bravo = new Bravo();
	int i = 0;

	public Control() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur
	 * 
	 * @param model
	 * @param alpha
	 * @param bravo
	 */

	public Control(Model model, Alpha alpha, Bravo bravo) {
		this.model = model;
		this.alpha = alpha;
		this.bravo = bravo;
		alpha.jtfPrenom.addKeyListener(this);
		alpha.jtfNomPaternel.addKeyListener(this);
		alpha.jtfNomMaternel.addKeyListener(this);
		alpha.jtfAge.addKeyListener(this);
		alpha.jtfNationalisation.addKeyListener(this);
		alpha.jtfCourrier.addKeyListener(this);
		alpha.jbtBoton.addActionListener(this);
		bravo.jcbOption.addItemListener(this);
	}

	/**
	 * Déclenche le flux principal du programme avec le bouton "Enregistrer"
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(alpha.jbtBoton)) {
			if (valider() == true) {
				if (enregistrer() == true) {
					alpha.jbtBoton.setText("finition");
					if ("finition".equals(alpha.jbtBoton.getText())) {
						finition();
					}
				}
			}
		}
	}

	/**
	 * Méthode pour valider toutes les TextFields
	 * 
	 * @return Valeur booléenne vrai quand toutes les validations sont corrects
	 */

	public boolean valider() {
		int age = 0;

		String prenom = alpha.jtfPrenom.getText().trim();
		String nomPaternel = alpha.jtfNomPaternel.getText().trim();
		String nomMaternel = alpha.jtfNomMaternel.getText().trim();
		String ageTexte = alpha.jtfAge.getText().trim();
		String nationalisation = alpha.jtfNationalisation.getText().trim();
		String courrier = alpha.jtfCourrier.getText().trim();

		// Motif pour valider l'e-mail
		Pattern pattern = Pattern.compile(
				"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

		// L'email à valider
		Matcher mather = pattern.matcher(courrier);

		// Commence à valider
		if (prenom == null || prenom.equals("") || prenom.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Entrez un prenom pour continuer");
			return false;
		} else if (nomPaternel == null || nomPaternel.equals("") || nomPaternel.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Entrez un nom de famille paternel pour continuer");
			return false;
		} else if (nomMaternel == null || nomMaternel.equals("") || nomMaternel.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Entrez un nom de famille maternel pour continuer");
			return false;
		} else if (nationalisation == null || nationalisation.equals("") || nationalisation.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Entrez une nationalisationalité pour continuer");
			return false;
		} else if (courrier == null || courrier.equals("") || courrier.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Entrez un courrier pour continuer");
			return false;
		} else if (mather.find() == false) {
			JOptionPane.showMessageDialog(null, "Entrez un courrier valide pour continuer");
			return false;
		} else if (ageTexte == null || ageTexte.equals("") || ageTexte.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Entrez un âge pour continuer");
			return false;
		} else {
			age = Integer.parseInt(ageTexte);
			if (age < 18 || age > 100) {
				JOptionPane.showMessageDialog(null, "Entrez un âge valide pour continuer");
				return false;
			}
		}

		// Ajouter les données à le model

		model.setPrenom(prenom);
		model.setNomPaternel(nomPaternel);
		model.setNomMaternel(nomMaternel);
		model.setAge(age);
		model.setNationalisation(nationalisation);
		model.setCourrier(courrier);

		return true;
	}

	/**
	 * Méthode suivant à valider en l'exécution
	 * 
	 * @return vrai pour enregistrer l'information sur l'arrangement chaque trois
	 *         fois
	 */

	public boolean enregistrer() {

		arrangement[i][0] = model.getPrenom();
		arrangement[i][1] = model.getNomPaternel();
		arrangement[i][2] = model.getNomMaternel();
		arrangement[i][3] = String.valueOf(model.getAge());
		arrangement[i][4] = model.getNationalisation();
		arrangement[i][5] = model.getCourrier();
		if (i < 2) {
			i++;
			model.setArray(arrangement);
			clair();
			alpha.jbtBoton.setText("Suivant"); // Changé le texte du bouton enregistrer pour Suivant
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Méthode finition pour appeler le nouvel vue "Bravo" puis de valider et
	 * ajouter tous le values sur de arrengement
	 */

	public void finition() {
		alpha.setVisible(false);
		bravo.setVisible(true);
	}

	@Override
	public void itemStateChanged(ItemEvent e) {

		if (bravo.jcbOption.getSelectedItem().equals("Apellidos")) {
			// Trier l'arrangement pour nom paternel
			boolean drapeau = true;
			String aux1 = null;
			String aux2 = null;
			String aux3 = null;
			String aux4 = null;
			String aux5 = null;
			String aux6 = null;

			while (drapeau) {
				drapeau = false;
				for (int i = 0; i < 2; i++) {
					if (arrangement[i][1].compareTo(arrangement[i + 1][1]) > 0) {
						aux1 = arrangement[i][0];
						aux2 = arrangement[i][1];
						aux3 = arrangement[i][2];
						aux4 = arrangement[i][3];
						aux5 = arrangement[i][4];
						aux6 = arrangement[i][5];

						arrangement[i][0] = arrangement[i + 1][0];
						arrangement[i][1] = arrangement[i + 1][1];
						arrangement[i][2] = arrangement[i + 1][2];
						arrangement[i][3] = arrangement[i + 1][3];
						arrangement[i][4] = arrangement[i + 1][4];
						arrangement[i][5] = arrangement[i + 1][5];

						arrangement[i + 1][0] = aux1;
						arrangement[i + 1][1] = aux2;
						arrangement[i + 1][2] = aux3;
						arrangement[i + 1][3] = aux4;
						arrangement[i + 1][4] = aux5;
						arrangement[i + 1][5] = aux6;
						model.setArray(arrangement);
					}
				}
				afficher(model.getArray());
			}
		} else if (bravo.jcbOption.getSelectedItem().equals("Edad")) {
			// Trier l'arrangement pour age
			boolean drapeau = true;
			String aux1 = null;
			String aux2 = null;
			String aux3 = null;
			String aux4 = null;
			String aux5 = null;
			String aux6 = null;

			while (drapeau) {
				drapeau = false;
				for (int i = 0; i < 2; i++) {
					if (arrangement[i][3].compareTo(arrangement[i + 1][3]) > 0) {
						aux1 = arrangement[i][0];
						aux2 = arrangement[i][1];
						aux3 = arrangement[i][2];
						aux4 = arrangement[i][3];
						aux5 = arrangement[i][4];
						aux6 = arrangement[i][5];

						arrangement[i][0] = arrangement[i + 1][0];
						arrangement[i][1] = arrangement[i + 1][1];
						arrangement[i][2] = arrangement[i + 1][2];
						arrangement[i][3] = arrangement[i + 1][3];
						arrangement[i][4] = arrangement[i + 1][4];
						arrangement[i][5] = arrangement[i + 1][5];

						arrangement[i + 1][0] = aux1;
						arrangement[i + 1][1] = aux2;
						arrangement[i + 1][2] = aux3;
						arrangement[i + 1][3] = aux4;
						arrangement[i + 1][4] = aux5;
						arrangement[i + 1][5] = aux6;

						model.setArray(arrangement);
					}
				}
				afficher(model.getArray());
			}
		} else if (bravo.jcbOption.getSelectedItem().equals("Nacionalidad")) {
			// Trier l'arrangement pour nationalité
			boolean drapeau = true;
			String aux1 = null;
			String aux2 = null;
			String aux3 = null;
			String aux4 = null;
			String aux5 = null;
			String aux6 = null;

			while (drapeau) {
				drapeau = false;
				for (int i = 0; i < 2; i++) {
					if (arrangement[i][4].compareTo(arrangement[i + 1][4]) > 0) {
						aux1 = arrangement[i][0];
						aux2 = arrangement[i][1];
						aux3 = arrangement[i][2];
						aux4 = arrangement[i][3];
						aux5 = arrangement[i][4];
						aux6 = arrangement[i][5];

						arrangement[i][0] = arrangement[i + 1][0];
						arrangement[i][1] = arrangement[i + 1][1];
						arrangement[i][2] = arrangement[i + 1][2];
						arrangement[i][3] = arrangement[i + 1][3];
						arrangement[i][4] = arrangement[i + 1][4];
						arrangement[i][5] = arrangement[i + 1][5];

						arrangement[i + 1][0] = aux1;
						arrangement[i + 1][1] = aux2;
						arrangement[i + 1][2] = aux3;
						arrangement[i + 1][3] = aux4;
						arrangement[i + 1][4] = aux5;
						arrangement[i + 1][5] = aux6;
						model.setArray(arrangement);
					}
				}
				afficher(model.getArray());
			}
		}
	}

	/**
	 * Méthode afficherai les données du arrangement
	 * 
	 * @param array
	 *            arrangement de type String
	 */

	public void afficher(String array[][]) {
		bravo.jtaInformation.setText("");
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				if (j <= 5) {
					bravo.jtaInformation.setText(bravo.jtaInformation.getText().toString() + "   " + array[i][j]);
				} else {
					bravo.jtaInformation.setText("\n");
				}
			}
		}
	}

	/**
	 * Méthode pour clair toutes les TextField
	 */

	public void clair() {
		alpha.jtfPrenom.setText(null);
		alpha.jtfNomPaternel.setText(null);
		alpha.jtfNomMaternel.setText(null);
		alpha.jtfAge.setText(null);
		alpha.jtfNationalisation.setText(null);
		alpha.jtfCourrier.setText(null);
	}

	/**
	 * Événements pour valider les clés
	 */

	@Override
	public void keyTyped(KeyEvent e) {
		if (e.getSource().equals(alpha.jtfAge)) {
			char c = e.getKeyChar();
			if (Character.isLetter(c)) {
				e.consume();
				JOptionPane.showMessageDialog(null, "Entrer seulement des nombres");
			}
		} else {
			char c = e.getKeyChar();
			if (Character.isDigit(c)) {
				e.consume();
				JOptionPane.showMessageDialog(null, "Entrez seulement des lettres");
			}
		}

	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
	}
}
