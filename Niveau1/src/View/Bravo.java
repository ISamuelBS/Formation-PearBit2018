package View;

import Controler.Control;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextPane;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class Bravo extends JFrame {

	private JPanel contentPane;
	public JComboBox jcbOption;
	public JTextPane jtaInformation;

	private JLabel jlbOption;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Bravo frame = new Bravo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Bravo() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel jpPanel = new JPanel();
		contentPane.add(jpPanel, BorderLayout.CENTER);
		jpPanel.setLayout(null);
		
		jlbOption = new JLabel("Selection de recherche:");
		jlbOption.setBounds(24, 33, 177, 33);
		jpPanel.add(jlbOption);
		
		jcbOption = new JComboBox();
		

		jcbOption.setModel(new DefaultComboBoxModel(new String[] {"Apellidos", "Edad", "Nacionalidad"}));
		jcbOption.setBounds(202, 37, 209, 29);
		jpPanel.add(jcbOption);
		
		jtaInformation = new JTextPane();
		jtaInformation.setBounds(24, 105, 387, 140);
		jpPanel.add(jtaInformation);
	}
}
