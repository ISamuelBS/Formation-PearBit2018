package modeles;

public class Telephone {

	private String numero;
	private String codePays;
	private String telephonique;
	
	public Telephone() {
		super();
	}

	public Telephone(String numero, String codePays, String telephonique) {
		super();
		this.numero = numero;
		this.codePays = codePays;
		this.telephonique = telephonique;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getCodePays() {
		return codePays;
	}

	public void setCodePays(String codePays) {
		this.codePays = codePays;
	}

	public String getTelephonique() {
		return telephonique;
	}

	public void setTelephonique(String telephonique) {
		this.telephonique = telephonique;
	}
}
