package Main;

import Controleur.Control;
import Modele.Etudiants;
import Modele.Personnes;
import Modele.Proffesseures;
import Vue.Alpha;

public class Main {

	public static void main(String[] args) {
		Alpha alpha = new Alpha();
		Etudiants etudiants = new Etudiants();
		Proffesseures proffesseures = new Proffesseures();
		Control control = new Control(etudiants, proffesseures, alpha);
		alpha.setVisible(true);
	}
}
