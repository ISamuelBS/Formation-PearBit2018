package Modele;

public class Personnes {

	private String prenom;
	private String nomPaternel;
	private String nomMaternel;
	private int age;
	
	public Personnes() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Personnes(String prenom, String nomPaternel, String nomMaternel, int age) {
		super();
		this.prenom = prenom;
		this.nomPaternel = nomPaternel;
		this.nomMaternel = nomMaternel;
		this.age = age;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNomPaternel() {
		return nomPaternel;
	}

	public void setNomPaternel(String nomPaternel) {
		this.nomPaternel = nomPaternel;
	}

	public String getNomMaternel() {
		return nomMaternel;
	}

	public void setNomMaternel(String nomMaternel) {
		this.nomMaternel = nomMaternel;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
}
