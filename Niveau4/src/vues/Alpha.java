package vues;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * 
 * @author pearbit-zero
 *
 */

public class Alpha extends JFrame {

	public JPanel contentPane;
	public JTextField jtfPrenom;
	public JTextField jtfNomPaternel;
	public JTextField jtfNomMaternel;
	public JTextField jtfAge;
	public JButton jbtSauvergarder;
	public JComboBox jcbSexe;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Alpha frame = new Alpha();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Alpha() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 250);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel jpPanel = new JPanel();
		contentPane.add(jpPanel, BorderLayout.CENTER);
		jpPanel.setLayout(null);
		
		JLabel jlbTitre = new JLabel("Enregistrer personnes");
		jlbTitre.setFont(new Font("Dialog", Font.BOLD, 12));
		jlbTitre.setBounds(113, 12, 188, 24);
		jpPanel.add(jlbTitre);
		
		JLabel jlbPrenom = new JLabel("Prenom:");
		jlbPrenom.setBounds(12, 39, 70, 15);
		jpPanel.add(jlbPrenom);
		
		JLabel jlbNomPaternel = new JLabel("Nom paternel:");
		jlbNomPaternel.setBounds(12, 66, 123, 15);
		jpPanel.add(jlbNomPaternel);
		
		JLabel jlbNomMaternel = new JLabel("Nom maternel:");
		jlbNomMaternel.setBounds(12, 93, 141, 15);
		jpPanel.add(jlbNomMaternel);
		
		JLabel jlbAge = new JLabel("Age:");
		jlbAge.setBounds(12, 120, 70, 15);
		jpPanel.add(jlbAge);
		
		JLabel jlbSexe = new JLabel("Sexe:");
		jlbSexe.setBounds(12, 147, 70, 15);
		jpPanel.add(jlbSexe);
		
		jtfPrenom = new JTextField();
		jtfPrenom.setBounds(143, 37, 114, 19);
		jpPanel.add(jtfPrenom);
		jtfPrenom.setColumns(10);
		
		jtfNomPaternel = new JTextField();
		jtfNomPaternel.setBounds(143, 64, 114, 19);
		jpPanel.add(jtfNomPaternel);
		jtfNomPaternel.setColumns(10);
		
		jtfNomMaternel = new JTextField();
		jtfNomMaternel.setBounds(143, 91, 114, 19);
		jpPanel.add(jtfNomMaternel);
		jtfNomMaternel.setColumns(10);
		
		jtfAge = new JTextField();
		jtfAge.setBounds(143, 120, 114, 19);
		jpPanel.add(jtfAge);
		jtfAge.setColumns(10);
		
		jcbSexe = new JComboBox();
		jcbSexe.setModel(new DefaultComboBoxModel(new String[] {"M", "F"}));
		jcbSexe.setBounds(143, 142, 114, 24);
		jpPanel.add(jcbSexe);
		
		jbtSauvergarder = new JButton("Sauvergarder");
		jbtSauvergarder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		jbtSauvergarder.setBounds(96, 178, 161, 25);
		jpPanel.add(jbtSauvergarder);
	}
}