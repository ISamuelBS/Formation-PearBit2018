package controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

import modeles.Personne;
import modeles.Telephone;
import vues.Alpha;
import vues.Bravo;
import vues.Charlie;

/**
 * Cette class s'appele "Control" parse que toute logic sur le project se trouver ici
 * 
 * @author pearbit-zero
 *
 */

public class Control extends Thread implements ActionListener, KeyListener {

	/**
	 * Initialiser les variables comme toutes les vues et les modeles relationés et un arrangement pour
	 * travailler avec le modele telephone
	 */
	
	Alpha alpha = new Alpha();
	Bravo bravo = new Bravo();
	Charlie charlie = new Charlie();

	Personne personne = new Personne();
	Telephone telephone = new Telephone();

	ArrayList<Telephone> tel = new ArrayList<Telephone>();
	
	/**
	 * Cette methode est le constructeur principale
	 */

	public Control() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Cette methode est le constructeur avec parameters
	 * @param alpha
	 * @param bravo
	 * @param charlie
	 * @param personne
	 * @param telephone
	 */
	
	public Control(Alpha alpha, Bravo bravo, Charlie charlie, Personne personne, Telephone telephone) {
		super();
		this.alpha = alpha;
		this.bravo = bravo;
		this.charlie = charlie;
		this.personne = personne;
		this.telephone = telephone;

		/**
		 * Toutes les components des vues 
		 */
		
		alpha.jbtSauvergarder.addActionListener(this);
		alpha.jcbSexe.addActionListener(this);
		alpha.jtfPrenom.addKeyListener(this);
		alpha.jtfNomPaternel.addKeyListener(this);
		alpha.jtfNomMaternel.addKeyListener(this);
		alpha.jtfAge.addKeyListener(this);

		bravo.jbtSauvegarder.addActionListener(this);
		bravo.jbtSuivant.addActionListener(this);
		bravo.jtfNumero.addKeyListener(this);
		bravo.jtfCodePays.addKeyListener(this);
		bravo.jtfTelephonique.addKeyListener(this);

		charlie.jbtAppeler.addActionListener(this);
	}

	/**
	 * Cette methode est l'évévenement de touts les boutons pour écouter environ le control et les actions 
	 * des vues
	 */
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (validerAlpha()) {
			if (e.getSource().equals(alpha.jbtSauvergarder)) {
				bravo.setVisible(true);
				alpha.setVisible(false);
			} else if (e.getSource().equals(bravo.jbtSauvegarder)) {
				if (validerBravo()) {
					clairBravo();
				}
			} else if (e.getSource().equals(bravo.jbtSuivant)) {
				charlie.setVisible(true);
				bravo.setVisible(false);
				remplirCombo();
			} else if (e.getSource().equals(charlie.jbtAppeler)) {
				if (charlie.jcbNumeros.equals(null)) {
					JOptionPane.showMessageDialog(null, "Il y a un problème avec l'événement");
				} else {
					start();
				}
			}
		}
	}

	/**
	 * Cette methode été implémenté pour utiliser fils et soulement traitement l'appel
	 */
	
	@Override
	public void run() {

		this.charlie.jtaInformation.setText("Bonjour et bienvenue à notre contrôle d'appel...");
		try {
			Thread.sleep(new Random().nextInt(10) * 1000);
			this.charlie.jtaInformation.setText("Appel de traitement, attendu");
			this.charlie.jtaInformation.setText("...");
			this.charlie.jtaInformation.setText("C'est tout, merci pour essayez notre système d'appel");
			this.charlie.jtaInformation.setText(":)");
		} catch (Exception e) {

		}
	}

	/**
	 * Cette methode se charge de remplir le combo de la vue "Charlie" avec tous le téléphones enregistré
	 */
	
	public void remplirCombo() {
		DefaultComboBoxModel<String> modelo = new DefaultComboBoxModel<String>();
		for (int i = 0; i < personne.getTelephones().size(); i++) {
			modelo.addElement(personne.getTelephones().get(i).getNumero());
		}
		charlie.jcbNumeros.setModel(modelo);
	}

	/**
	 * Cette methode uniquement valide tous les zones de texte de la vue "Alpha" et enregistré le contenu 
	 * sur le modele Personne
	 * @return vrai si toutes les zones de texte ne présentent aucun problème
	 */
	
	public boolean validerAlpha() {
		int age = 0;

		String prenom = alpha.jtfPrenom.getText().trim();
		String nomPaternel = alpha.jtfNomPaternel.getText().trim();
		String nomMaternel = alpha.jtfNomMaternel.getText().trim();
		String ageTexte = alpha.jtfAge.getText().trim();
		String sexe = (String) alpha.jcbSexe.getSelectedItem();

		// Commence à valider
		if (prenom == null || prenom.equals("") || prenom.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Entrez un prenom pour continuer");
			return false;
		} else if (nomPaternel == null || nomPaternel.equals("") || nomPaternel.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Entrez un nom de famille paternel pour continuer");
			return false;
		} else if (nomMaternel == null || nomMaternel.equals("") || nomMaternel.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Entrez un nom de famille maternel pour continuer");
			return false;
		} else if (ageTexte == null || ageTexte.equals("") || ageTexte.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Entrez un âge pour continuer");
			return false;
		} else {
			age = Integer.parseInt(ageTexte);
			if (age < 18 || age > 100) {
				JOptionPane.showMessageDialog(null, "Entrez un âge valide pour continuer");
				return false;
			}
		}

		// Ajouter les données à le modele

		personne.setPrenom(prenom);
		personne.setNomPaternel(nomPaternel);
		personne.setNomMaternel(nomMaternel);
		personne.setAge(age);
		personne.setSexe(sexe);
		return true;
	}

	/**
	 * Cette methode uniquement valide tous les zones de texte de la vue "Bravo" et enregistré le contenu 
	 * sur le modele Telephone, aussi s'enregistré comme object sur le arrangement Telephones du modele 
	 * Personne
	 * @return vrai si toutes les zones de texte ne présentent aucun problème
	 */
	
	public boolean validerBravo() {

		String numero = bravo.jtfNumero.getText().trim();
		String codePays = bravo.jtfCodePays.getText().trim();
		String telephonique = bravo.jtfTelephonique.getText().trim();

		// Commence à valider
		if (numero == null || numero.equals("") || numero.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Entrez un numéro du téléphone pour continuer");
			return false;
		} else if (codePays == null || codePays.equals("") || codePays.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Entrez un code du pays pour continuer");
			return false;
		} else if (telephonique == null || telephonique.equals("") || telephonique.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Entrez un nom du téléphonique pour continuer");
			return false;
		}

		// Ajouter les données à le model

		telephone.setNumero(numero);
		telephone.setCodePays(codePays);
		telephone.setTelephonique(telephonique);

		tel = personne.getTelephones();
		tel.add(telephone);
		personne.setTelephones(tel);
		return true;
	}

	/**
	 * Cette methode uniquement  clair toutes les zones texte de la vue "Bravo"
	 */
	public void clairBravo() {
		bravo.jtfNumero.setText(null);
		bravo.jtfCodePays.setText(null);
		bravo.jtfTelephonique.setText(null);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		if (e.getSource().equals(alpha.jtfAge) || e.getSource().equals(bravo.jtfNumero)
				|| e.getSource().equals(bravo.jtfCodePays)) {
			char c = e.getKeyChar();
			if (Character.isLetter(c)) {
				e.consume();
				JOptionPane.showMessageDialog(null, "Entrer seulement des nombres");
			}
		} else {
			char c = e.getKeyChar();
			if (Character.isDigit(c)) {
				e.consume();
				JOptionPane.showMessageDialog(null, "Entrez seulement des lettres");
			}
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}
}
