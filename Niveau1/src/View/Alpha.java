package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.FlowLayout;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

import Controler.Control;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Alpha extends JFrame {

	private JPanel contentPane;
	public JTextField jtfPrenom;
	public JTextField jtfNomPaternel;
	public JTextField jtfNomMaternel;
	public JTextField jtfAge;
	public JTextField jtfNationalisation;
	public JTextField jtfCourrier;
	public JButton jbtBoton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Alpha frame = new Alpha();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Alpha() {
		setTitle("Niveu 1");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 445, 319);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel jpPanel = new JPanel();
		contentPane.add(jpPanel, BorderLayout.CENTER);
		jpPanel.setLayout(null);
		
		JLabel jlbPrenom = new JLabel("Prenom:");
		jlbPrenom.setBounds(32, 23, 59, 15);
		jpPanel.add(jlbPrenom);
		
		JLabel lblNomPaternel = new JLabel("Nom Paternel:");
		lblNomPaternel.setBounds(32, 50, 101, 15);
		jpPanel.add(lblNomPaternel);
		
		JLabel lblNomMaternel = new JLabel("Nom Maternel:");
		lblNomMaternel.setBounds(32, 77, 104, 15);
		jpPanel.add(lblNomMaternel);
		
		JLabel jlbAge = new JLabel("Age:");
		jlbAge.setBounds(32, 104, 70, 15);
		jpPanel.add(jlbAge);
		
		JLabel lblNationalitation = new JLabel("Nationalisation:");
		lblNationalitation.setBounds(32, 131, 141, 15);
		jpPanel.add(lblNationalitation);
		
		JLabel jlbCourrier = new JLabel("Courrier:");
		jlbCourrier.setBounds(32, 158, 70, 15);
		jpPanel.add(jlbCourrier);
		
		jtfPrenom = new JTextField();

		jtfPrenom.setBounds(204, 21, 114, 19);
		jpPanel.add(jtfPrenom);
		jtfPrenom.setColumns(10);
		
		jtfNomPaternel = new JTextField();

		jtfNomPaternel.setBounds(204, 50, 114, 19);
		jpPanel.add(jtfNomPaternel);
		jtfNomPaternel.setColumns(10);
		
		jtfNomMaternel = new JTextField();

		jtfNomMaternel.setBounds(204, 75, 114, 19);
		jpPanel.add(jtfNomMaternel);
		jtfNomMaternel.setColumns(10);
		
		jtfAge = new JTextField();

		jtfAge.setBounds(204, 102, 114, 19);
		jpPanel.add(jtfAge);
		jtfAge.setColumns(10);
		
		jtfNationalisation = new JTextField();

		jtfNationalisation.setBounds(204, 129, 114, 19);
		jpPanel.add(jtfNationalisation);
		jtfNationalisation.setColumns(10);
		
		jtfCourrier = new JTextField();

		jtfCourrier.setBounds(204, 156, 114, 19);
		jpPanel.add(jtfCourrier);
		jtfCourrier.setColumns(10);
		
		jbtBoton = new JButton("Enregistrer");

		jbtBoton.setBounds(204, 216, 117, 25);
		jpPanel.add(jbtBoton);
	}
}
