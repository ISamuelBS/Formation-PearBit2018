package vues;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.JLabel;

/**
 * 
 * @author pearbit-zero
 *
 */

public class Charlie extends JFrame {

	public JPanel contentPane;
	public JButton jbtAppeler;
	public JTextArea jtaInformation;
	public JComboBox jcbNumeros;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Charlie frame = new Charlie();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Charlie() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 460, 266);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel jpPanel = new JPanel();
		contentPane.add(jpPanel, BorderLayout.CENTER);
		jpPanel.setLayout(null);
		
		JLabel jlbNumeros = new JLabel("Numéros:");
		jlbNumeros.setBounds(38, 41, 70, 15);
		jpPanel.add(jlbNumeros);
		
		JLabel jlbTitre = new JLabel("Appel test...");
		jlbTitre.setBounds(167, 0, 180, 25);
		jpPanel.add(jlbTitre);
		
		jcbNumeros = new JComboBox();
		jcbNumeros.setBounds(124, 32, 134, 24);
		jpPanel.add(jcbNumeros);
		
		jbtAppeler = new JButton("Appeler");
		jbtAppeler.setBounds(298, 32, 117, 25);
		jpPanel.add(jbtAppeler);
		
		jtaInformation = new JTextArea();
		jtaInformation.setBounds(38, 69, 377, 134);
		jpPanel.add(jtaInformation);
	}
}