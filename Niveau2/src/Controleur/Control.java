package Controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JTextField;

import Modele.Etudiants;
import Modele.Personnes;
import Modele.Proffesseures;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import Vue.Alpha;

/**
 * Cette classe est le contrôleur principal et implémente différents événements
 * 
 * @author pearbit-zero
 * @version 1.0
 */

public class Control implements ActionListener, KeyListener, ItemListener {

	/**
	 * Initialiser les vues et les modèles
	 */

	Etudiants etudiants = new Etudiants();
	Proffesseures proffeseures = new Proffesseures();
	Alpha alpha = new Alpha();
	ArrayList<String> arrangementEtudiant = new ArrayList<String>();
	ArrayList<String> arrangementProfesseur = new ArrayList<String>();

	// Opérateur itératif global
	int i = 0;

	/**
	 * Constructeur par défaut
	 */

	public Control() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur avec paramètres à general differents elements du vue alpha
	 * 
	 * @param etudiants
	 * @param proffeseures
	 * @param alpha
	 */

	public Control(Etudiants etudiants, Proffesseures proffeseures, Alpha alpha) {
		this.etudiants = etudiants;
		this.proffeseures = proffeseures;
		this.alpha = alpha;

		alpha.jrbEtudiant.addActionListener(this);
		alpha.jrbProffesseur.addActionListener(this);
		alpha.jtfPrenom.addActionListener(this);
		alpha.jtfNomPaternel.addActionListener(this);
		alpha.jtfNomMaternel.addActionListener(this);
		alpha.jtfAge.addActionListener(this);

		alpha.jtfPrenom.addKeyListener(this);
		alpha.jtfNomPaternel.addKeyListener(this);
		alpha.jtfNomMaternel.addKeyListener(this);
		alpha.jtfAge.addKeyListener(this);
		alpha.jtfTexte1.addKeyListener(this);
		alpha.jtfTexte2.addKeyListener(this);
		alpha.jtfTexte3.addKeyListener(this);

		alpha.jbtEnregistrer.addActionListener(this);
		alpha.jcbOptions.addItemListener(this);
	}

	/**
	 * Événement principal du boutton enregistrer
	 */

	@Override
	public void actionPerformed(ActionEvent e) {
		if (echange()) {
			rendreVisible();
			if (e.getSource().equals(alpha.jbtEnregistrer)) {
				if (alpha.jrbProffesseur.isSelected()) {
					if (validerProffesseur()) {
						if (enregistrerProffesseures()) {
							JOptionPane.showMessageDialog(null, "Professeur enregistre!");
							alpha.jcbOptions.enable(true);
						} // Enregistre le prefesseur
					} // Validation du professeur VRAI
					else {
						JOptionPane.showMessageDialog(null, "Problème lors de la validation des données du professeur");
					} // Validation du professeur FAUX
				} // Radio Professeur selected
				else if (alpha.jrbEtudiant.isSelected()) {
					if (validerEtudiant()) {
						if (enregistrerEtudiants()) {
							JOptionPane.showMessageDialog(null, "Étudiant enregistre!");
							alpha.jcbOptions.enable(true);
						} // Enregistre l'étudiant
					} else {
						JOptionPane.showMessageDialog(null, "Problème lors de la validation des données de l'étudiant");
					} // Validation d'étudiant
				} // Radio Etudiant selected
			} // Bouton Enregistrer
		} // echange
	} // actionPerfomed

	/**
	 * Méthode pour valider toutes les TextFields exclusif des proffessuers
	 * 
	 * @return Valeur booléenne vrai quand toutes les validations sont corrects
	 */

	public boolean validerProffesseur() {
		int age = 0;
		int nombreCabine = 0;

		String prenom = alpha.jtfPrenom.getText().trim();
		String nomPaternel = alpha.jtfNomPaternel.getText().trim();
		String nomMaternel = alpha.jtfNomMaternel.getText().trim();
		String ageTexte = alpha.jtfAge.getText().trim();
		String cedule = alpha.jtfTexte1.getText().trim();
		String specialite = alpha.jtfTexte2.getText().trim();
		String nombreCabineTexte = alpha.jtfTexte3.getText().trim();

		// Commence à valider
		if (prenom == null || prenom.equals("") || prenom.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Entrez un prenom pour continuer");
			return false;
		} else if (nomPaternel == null || nomPaternel.equals("") || nomPaternel.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Entrez un nom de famille paternel pour continuer");
			return false;
		} else if (nomMaternel == null || nomMaternel.equals("") || nomMaternel.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Entrez un nom de famille maternel pour continuer");
			return false;
		} else if (ageTexte == null || ageTexte.equals("") || ageTexte.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Entrez un âge pour continuer");
			return false;
		} else if (cedule == null || cedule.equals("") || cedule.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Entrez une cedule pour continuer");
			return false;
		} else if (specialite == null || specialite.equals("") || specialite.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Entrez un specialite pour continuer");
			return false;
		} else if (nombreCabineTexte == null || nombreCabineTexte.equals("") || nombreCabineTexte.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Entrez un nombre de cabine pour continuer");
			return false;
		} else {
			age = Integer.parseInt(ageTexte);
			nombreCabine = Integer.parseInt(nombreCabineTexte);

			if (age < 18 || age > 100) {
				JOptionPane.showMessageDialog(null, "Entrez un âge valide pour continuer");
				return false;
			} else if (nombreCabine < 1 || nombreCabine > 200) {
				JOptionPane.showMessageDialog(null, "Entrez un nombre de cabine valide pour continuer");
				return false;
			}
		}

		// Ajouter les données à le model

		proffeseures.setPrenom(prenom);
		proffeseures.setNomPaternel(nomPaternel);
		proffeseures.setNomMaternel(nomMaternel);
		proffeseures.setAge(age);
		proffeseures.setCedule(cedule);
		proffeseures.setSpecialite(specialite);
		proffeseures.setNombreCabine(nombreCabine);
		return true;
	}

	/**
	 * Méthode pour valider toutes les TextFields exclusif des étudiants
	 * 
	 * @return Valeur booléenne vrai quand toutes les validations sont corrects
	 */

	public boolean validerEtudiant() {
		int age = 0;

		String prenom = alpha.jtfPrenom.getText().trim();
		String nomPaternel = alpha.jtfNomPaternel.getText().trim();
		String nomMaternel = alpha.jtfNomMaternel.getText().trim();
		String ageTexte = alpha.jtfAge.getText().trim();
		String matricule = alpha.jtfTexte1.getText().trim();
		String generation = alpha.jtfTexte2.getText().trim();
		String horaire = alpha.jtfTexte3.getText().trim();

		// Commence à valider
		if (prenom == null || prenom.equals("") || prenom.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Entrez un prenom pour continuer");
			return false;
		} else if (nomPaternel == null || nomPaternel.equals("") || nomPaternel.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Entrez un nom de famille paternel pour continuer");
			return false;
		} else if (nomMaternel == null || nomMaternel.equals("") || nomMaternel.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Entrez un nom de famille maternel pour continuer");
			return false;
		} else if (ageTexte == null || ageTexte.equals("") || ageTexte.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Entrez un âge pour continuer");
			return false;
		} else if (matricule == null || matricule.equals("") || matricule.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Entrez une matricule pour continuer");
			return false;
		} else if (generation == null || generation.equals("") || generation.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Entrez une generation pour continuer");
			return false;
		} else if (horaire == null || horaire.equals("") || horaire.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Entrez un horaire pour continuer");
			return false;
		} else {
			age = Integer.parseInt(ageTexte);
			if (age < 18 || age > 100) {
				JOptionPane.showMessageDialog(null, "Entrez un âge valide pour continuer");
				return false;
			}
		}

		// Ajouter les données à le model

		etudiants.setPrenom(prenom);
		etudiants.setNomPaternel(nomPaternel);
		etudiants.setNomMaternel(nomMaternel);
		etudiants.setAge(age);
		etudiants.setMatricule(matricule);
		etudiants.setGeneration(generation);
		etudiants.setHoraire(horaire);
		return true;
	}

	/**
	 * Cette methode fait les differents echangements selon le option selectionnée
	 * 
	 * @return vrai, en fonction des radio Bouttons
	 */

	public boolean echange() {

		if (alpha.jrbProffesseur.isSelected()) {
			alpha.lblTexte1.setText("Cedule:");
			alpha.lblTexte2.setText("Specialité:");
			alpha.lblTexte3.setText("Nombre de cabine:");
			return true;
		} else if (alpha.jrbEtudiant.isSelected()) {
			alpha.jtfTexte1.setVisible(true);
			alpha.jtfTexte2.setVisible(true);
			alpha.jtfTexte3.setVisible(true);
			alpha.lblTexte1.setText("Matricule:");
			alpha.lblTexte2.setText("Generation:");
			alpha.lblTexte3.setText("Horaire:");
			return true;
		} else {
			JOptionPane.showMessageDialog(null, "Vous n'avez pas selectioné une option");
			return false;
		}
	}
	
	/**
	 * Cette methode afficher tous les values enregistré 
	 * @param array
	 */

	public void afficher(ArrayList array) {

		alpha.jtaInformation.setVisible(true);

		Iterator<String> NomIterateur = array.iterator();

		while (NomIterateur.hasNext()) {
			if (i <= 6) {
				String element = NomIterateur.next();
				System.out.print(element + " | ");
				alpha.jtaInformation.setText(alpha.jtaInformation.getText().toString() + "   " + element + " | ");
				i++;
			} else {
				i = 0;
				System.out.println("\n");
				alpha.jtaInformation.setText(alpha.jtaInformation.getText().toString() + "\n");
			}
		}
	}

	/**
	 * Cette methode afficher les differents zones de texte (JTextFields)
	 */
	
	public void rendreVisible() {
		alpha.jtfTexte1.setVisible(true);
		alpha.jtfTexte2.setVisible(true);
		alpha.jtfTexte3.setVisible(true);
	}

	/**
	 * Cette methode clair les differents zones de texte de la vue
	 */
	
	public void clair() {
		alpha.jtfPrenom.setText(null);
		alpha.jtfNomPaternel.setText(null);
		alpha.jtfNomMaternel.setText(null);
		alpha.jtfAge.setText(null);
		alpha.jtfTexte1.setText(null);
		alpha.jtfTexte2.setText(null);
		alpha.jtfTexte3.setText(null);
	}
	
	/**
	 * Cette methode represanté l'événement du type keyTiped pour valider les zones de texte
	 */

	@Override
	public void keyTyped(KeyEvent e) {
		if (e.getSource().equals(alpha.jtfAge)
				|| (e.getSource().equals(alpha.jtfTexte3) && alpha.jrbProffesseur.isSelected())) {
			char c = e.getKeyChar();
			if (Character.isLetter(c)) {
				e.consume();
				JOptionPane.showMessageDialog(null, "Entrer seulement des nombres");
			}
		} else {
			char c = e.getKeyChar();
			if (Character.isDigit(c)) {
				e.consume();
				JOptionPane.showMessageDialog(null, "Entrez seulement des lettres");
			}
		}
	}

	/**
	 * 
	 * @return vrai, quand le proffesseur s'enregistr�
	 */
	
	public boolean enregistrerProffesseures() {
		arrangementProfesseur.add(proffeseures.getPrenom());
		arrangementProfesseur.add(proffeseures.getNomPaternel());
		arrangementProfesseur.add(proffeseures.getNomMaternel());
		arrangementProfesseur.add(String.valueOf(proffeseures.getAge()));
		arrangementProfesseur.add(proffeseures.getCedule());
		arrangementProfesseur.add(proffeseures.getSpecialite());
		arrangementProfesseur.add(String.valueOf(proffeseures.getNombreCabine()));

		proffeseures.setProfesseurs(arrangementProfesseur);

		alpha.jbtEnregistrer.setText("Enregistrer outre"); // Changé le texte du bouton enregistrer pour Enrehgistrer
															// outre proffesseure
		clair();
		return true;
	}

	/**
	 * 
	 * @return vrai, quand l'�tudiant s'enregistr�
	 */
	
	public boolean enregistrerEtudiants() {

		arrangementEtudiant.add(etudiants.getPrenom());
		arrangementEtudiant.add(etudiants.getNomPaternel());
		arrangementEtudiant.add(etudiants.getNomMaternel());
		arrangementEtudiant.add(String.valueOf(etudiants.getAge()));
		arrangementEtudiant.add(etudiants.getMatricule());
		arrangementEtudiant.add(etudiants.getGeneration());
		arrangementEtudiant.add(etudiants.getHoraire());

		etudiants.setEtudiants(arrangementEtudiant);
		alpha.jbtEnregistrer.setText("Enregistrer outre"); // Changé le texte du bouton enregistrer pour Enrehgistrer
															// outre proffesseure
		clair();
		return true;
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	/**
	 * 
	 */
	
	@Override
	public void itemStateChanged(ItemEvent e) {
		if (alpha.jcbOptions.getSelectedItem().equals("Afficher etudiants")) {

			if (etudiants.getEtudiants().isEmpty()) {
				JOptionPane.showMessageDialog(null, "Il n'y a pas d'�tudiants inscrits");
			} else {
				afficher(etudiants.getEtudiants());
			}
		} else if (alpha.jcbOptions.getSelectedItem().equals("Afficher proffesseures")) {

			if (proffeseures.getProfesseurs().isEmpty()) {
				JOptionPane.showMessageDialog(null, "Il n'y a pas de professeures inscrits");
			} else {
				afficher(proffeseures.getProfesseurs());
			}
		} else if (alpha.jcbOptions.getSelectedItem().equals("Afficher tous")) {
			System.out.println("Muestra todos");
		}
	}
}