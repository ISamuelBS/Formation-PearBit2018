package Model;

public class Model {

	/**
	 * Class Model pour manipuler toute l'information sut le personnes
	 */
	
    public String prenom;
    public String nomPaternel;
    public String nomMaternel;
    public int age;
    public String nationalisation;
    public String courrier;
    public String array[][];
    
    /**
     * Constructeur par defaut
     */
    
	public Model() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur avec parametres 
	 * @param prenom
	 * @param nomPaternel
	 * @param nomMaternel
	 * @param age
	 * @param nationalisation
	 * @param courrier
	 * @param array
	 */
	
	public Model(String prenom, String nomPaternel, String nomMaternel, int age, String nationalisation,
			String courrier, String[][] array) {
		super();
		this.prenom = prenom;
		this.nomPaternel = nomPaternel;
		this.nomMaternel = nomMaternel;
		this.age = age;
		this.nationalisation = nationalisation;
		this.courrier = courrier;
		this.array = array;
	}

	/**
	 * Méthodes gets et sets
	 * @return
	 */
	
	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNomPaternel() {
		return nomPaternel;
	}

	public void setNomPaternel(String nomPaternel) {
		this.nomPaternel = nomPaternel;
	}

	public String getNomMaternel() {
		return nomMaternel;
	}

	public void setNomMaternel(String nomMaternel) {
		this.nomMaternel = nomMaternel;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getNationalisation() {
		return nationalisation;
	}

	public void setNationalisation(String nationalisation) {
		this.nationalisation = nationalisation;
	}

	public String getCourrier() {
		return courrier;
	}

	public void setCourrier(String courrier) {
		this.courrier = courrier;
	}

	public String[][] getArray() {
		return array;
	}

	public void setArray(String[][] array) {
		this.array = array;
	}
}
