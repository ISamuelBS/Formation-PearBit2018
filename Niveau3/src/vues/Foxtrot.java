package vues;

import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JButton;

/**
 * 
 * @author pearbit-zero
 *
 */


public class Foxtrot extends JFrame {

	public JPanel contentPane;
	public JComboBox jcbSujet;
	public JButton jbtSauvegarder;
	public JButton jbtRetourner;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Foxtrot frame = new Foxtrot();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Foxtrot() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 577, 182);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel jpPanel = new JPanel();
		contentPane.add(jpPanel, BorderLayout.CENTER);
		jpPanel.setLayout(null);
		
		JLabel jlbTitre = new JLabel("Enregistrer groupes");
		jlbTitre.setBounds(140, 37, 158, 15);
		jpPanel.add(jlbTitre);
		
		JLabel jlbSujet = new JLabel("Sujets:");
		jlbSujet.setBounds(33, 69, 113, 15);
		jpPanel.add(jlbSujet);
		
		jcbSujet = new JComboBox();
		jcbSujet.setBounds(140, 64, 158, 24);
		jpPanel.add(jcbSujet);
		
		jbtSauvegarder = new JButton("Sauvegarder");
		jbtSauvegarder.setBounds(326, 32, 150, 25);
		jpPanel.add(jbtSauvegarder);
		
		jbtRetourner = new JButton("Retourner");
		jbtRetourner.setBounds(326, 86, 150, 25);
		jpPanel.add(jbtRetourner);
	}
}
