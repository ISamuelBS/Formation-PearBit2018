package modeles;

public class Personne {

	private String prenom;
	private String nomPaternel;
	private String NomMaternel;
	private String dateNaissance;
	
	public Personne() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Personne(String prenom, String nomPaternel, String nomMaternel, String dateNaissance) {
		super();
		this.prenom = prenom;
		this.nomPaternel = nomPaternel;
		NomMaternel = nomMaternel;
		this.dateNaissance = dateNaissance;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNomPaternel() {
		return nomPaternel;
	}

	public void setNomPaternel(String nomPaternel) {
		this.nomPaternel = nomPaternel;
	}

	public String getNomMaternel() {
		return NomMaternel;
	}

	public void setNomMaternel(String nomMaternel) {
		NomMaternel = nomMaternel;
	}

	public String getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(String dateNaissance) {
		this.dateNaissance = dateNaissance;
	}	
}
