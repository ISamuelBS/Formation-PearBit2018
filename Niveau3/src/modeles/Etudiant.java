package modeles;

public class Etudiant extends Personne{

	private int idEtudiant;
	private String matricule;
	
	public Etudiant() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Etudiant(String prenom, String nomPaternel, String nomMaternel, String dateNaissance) {
		super(prenom, nomPaternel, nomMaternel, dateNaissance);
		// TODO Auto-generated constructor stub
	}

	public Etudiant(int idEtudiant, String matricule) {
		super();
		this.idEtudiant = idEtudiant;
		this.matricule = matricule;
	}
	public int getIdEtudiant() {
		return idEtudiant;
	}
	public void setIdEtudiant(int idEtudiant) {
		this.idEtudiant = idEtudiant;
	}
	public String getMatricule() {
		return matricule;
	}
	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}
}
