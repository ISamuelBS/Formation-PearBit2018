package modeles;

public class Departament {

	
	private int idDepartement;
	private String nomDepartement;
	
	public Departament() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Departament(int idDepartement, String nomDepartement) {
		super();
		this.idDepartement = idDepartement;
		this.nomDepartement = nomDepartement;
	}

	public int getIdDepartement() {
		return idDepartement;
	}

	public void setIdDepartement(int idDepartement) {
		this.idDepartement = idDepartement;
	}

	public String getNomDepartement() {
		return nomDepartement;
	}

	public void setNomDepartement(String nomDepartement) {
		this.nomDepartement = nomDepartement;
	}
}
