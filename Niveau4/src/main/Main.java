package main;

import controleur.Control;
import modeles.Personne;
import modeles.Telephone;
import vues.Alpha;
import vues.Bravo;
import vues.Charlie;

public class Main {

	public static void main(String[] args) {

		Alpha alpha = new Alpha();
		Bravo bravo = new Bravo();
		Charlie charlie = new Charlie();

		Personne personne = new Personne();
		Telephone telephone = new Telephone();

		Control control = new Control(alpha, bravo, charlie, personne, telephone);

		alpha.setVisible(true);
	}

}
