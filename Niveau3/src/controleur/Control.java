package controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

import connection.Conect;
import modeles.Classe;
import modeles.Departament;
import modeles.Etudiant;
import modeles.Groupe;
import modeles.Professeur;
import modeles.Sujet;
import vues.Alpha;
import vues.Bravo;
import vues.Charlie;
import vues.Delta;
import vues.Echo;
import vues.Foxtrot;
import vues.Golf;
public class Control implements ActionListener {

	Alpha alpha = new Alpha();
	Bravo bravo = new Bravo();
	Charlie charlie = new Charlie();
	Delta delta = new Delta();
	Echo echo = new Echo();
	Foxtrot foxtrot = new Foxtrot();
	Golf golf = new Golf();

	Classe classe = new Classe();
	Departament departament = new Departament();
	Etudiant etudiant = new Etudiant();
	Groupe groupe = new Groupe();
	Professeur professeur = new Professeur();
	Sujet sujet = new Sujet();

	Conect conect = new Conect();

	String nombre = null;

	public Control() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Control(Alpha alpha, Classe classe, Departament departament, Etudiant etudiant, Groupe groupe,
			Professeur professeur, Sujet sujet) {
		super();
		this.alpha = alpha;
		this.classe = classe;
		this.departament = departament;
		this.etudiant = etudiant;
		this.groupe = groupe;
		this.professeur = professeur;
		this.sujet = sujet;

		alpha.jbtClasse.addActionListener(this);
		alpha.jbtDepartament.addActionListener(this);
		alpha.jbtEtudiant.addActionListener(this);
		alpha.jbtGroupe.addActionListener(this);
		alpha.jbtProfesseur.addActionListener(this);
		alpha.jbtSujet.addActionListener(this);

		bravo.jbtModifier.addActionListener(this);
		bravo.jbtSupprimer.addActionListener(this);

		delta.jbtModifier.addActionListener(this);
		delta.jbtSupprimer.addActionListener(this);

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource().equals(alpha.jbtEtudiant)) {
			alpha.setVisible(false);
			bravo.setVisible(true);
		}

	}

	// BRAVO ETUDIANT

	public String WOEspacio(String WOS) {
		String vWOS = WOS.replaceAll(" +", " ");
		return vWOS;
	}

	public void insertarBravo() {

		try {
			if (bravo.jtfMatricule.getText().isEmpty() | nombre.isEmpty() | bravo.jtfNomPaternel.getText().isEmpty()
					| bravo.jtfNomMaternel.getText().isEmpty() | bravo.jtfDateNaissance.getText().isEmpty()) {
				JOptionPane.showMessageDialog(null, "Existen campos vacios.", "Alerta",
						JOptionPane.INFORMATION_MESSAGE);
			} else {

				etudiant.setMatricule(WOEspacio(bravo.jtfMatricule.getText().trim()));
				etudiant.setPrenom(WOEspacio(bravo.jtfPrenom.getText().trim()));
				etudiant.setNomPaternel(WOEspacio(bravo.jtfNomPaternel.getText().trim()));
				etudiant.setNomMaternel(WOEspacio(bravo.jtfNomMaternel.getText().trim()));
				etudiant.setDateNaissance(WOEspacio(bravo.jtfDateNaissance.getText().trim()));

				String instruccionDB = "INSERT INTO alumno VALUES(NULL,'" + etudiant.getMatricule() + "','"
						+ etudiant.getPrenom() + "','" + etudiant.getNomPaternel() + "','" + etudiant.getNomMaternel()
						+ "'," + "'" + etudiant.getDateNaissance() + "')";
				PreparedStatement ps = conect.getConexion().prepareStatement(instruccionDB);
				ps.execute();
				JOptionPane.showMessageDialog(null, "Registro agregado", "Query OK", JOptionPane.INFORMATION_MESSAGE);
				limpiarBravo();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void consultarBravo(String msj) {

		try {

			etudiant.setMatricule(JOptionPane.showInputDialog(null, "Ingresa la matricula a " + msj));
			String instruccionBD = "SELECT * FROM alumno WHERE matricula='" + etudiant.getMatricule() + "'";
			conect.Conectar();
			PreparedStatement ps = conect.getConexion().prepareStatement(instruccionBD);
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				bravo.jtfMatricule.setText(rs.getString(2));
				bravo.jtfPrenom.setText(rs.getString(3));
				bravo.jtfNomPaternel.setText(rs.getString(4));
				bravo.jtfNomMaternel.setText(rs.getString(5));
				bravo.jtfDateNaissance.setText(rs.getString(6));

			} else {
				JOptionPane.showMessageDialog(null, "El usuario no existe", "Alerta", JOptionPane.INFORMATION_MESSAGE);
			}

		} catch (SQLException ex) {
			JOptionPane.showMessageDialog(null, "error" + ex);
		}
	}

	public void modificarBravo() {

		try {
			if (bravo.jbtModifier.getText().equals("Modificar")) {
				bravo.jbtModifier.setText("Confirmar");
				consultarBravo("a modificar");
			} else {
				String instruccionDB = "UPDATE alumno SET matricula='" + WOEspacio(bravo.jtfMatricule.getText().trim())
						+ "',nombre='" + WOEspacio(bravo.jtfPrenom.getText().trim()) + "',aPaterno='"
						+ WOEspacio(bravo.jtfNomPaternel.getText().trim()) + "',aMaterno='"
						+ WOEspacio(bravo.jtfNomMaternel.getText().trim()) + "',fechaNacimiento='"
						+ WOEspacio(bravo.jtfDateNaissance.getText().trim()) + "'WHERE matricula='"
						+ bravo.jtfMatricule.getText() + "'";

				PreparedStatement ps = conect.getConexion().prepareStatement(instruccionDB);
				ps.execute();
				bravo.jbtModifier.setText("Modifier");
				JOptionPane.showMessageDialog(null, "Registro modificado", "Query OK", JOptionPane.INFORMATION_MESSAGE);
				limpiarBravo();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void eliminarBravo() {

		try {
			if (bravo.jbtSupprimer.getText().equals("Eliminar")) {
				consultarBravo("Eliminar");
				bravo.jbtSupprimer.setText("Confirmar");
			} else {

				String instruccionBD = "DELETE FROM alumno WHERE matricula='" + bravo.jtfMatricule.getText() + "'";
				PreparedStatement ps = conect.getConexion().prepareStatement(instruccionBD);
				ps.execute();
				bravo.jbtSupprimer.setText("Eliminar");
				limpiarBravo();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public void limpiarBravo() {
		bravo.jtfMatricule.setText(null);
		bravo.jtfPrenom.setText(null);
		bravo.jtfNomPaternel.setText(null);
		bravo.jtfNomMaternel.setText(null);
		bravo.jtfDateNaissance.setText(null);
	}

	// CHARLIE DEPARTAMENT

	public void insertarCharlie() {

		try {
			departament.setNomDepartement(WOEspacio(charlie.jtfNomDepartament.getText().trim()));
			if (charlie.jtfNomDepartament.getText().isEmpty()) {
				JOptionPane.showMessageDialog(null, "Existen campos vacios.", "Alerta",
						JOptionPane.INFORMATION_MESSAGE);
			} else {

				String instruccionDB = "INSERT INTO departamento VALUES(NULL, '" + charlie.jtfNomDepartament.getText()
						+ "')";

				PreparedStatement ps = conect.getConexion().prepareStatement(instruccionDB);
				ps.execute();
				JOptionPane.showMessageDialog(null, "Registro agregado", "Query OK", JOptionPane.INFORMATION_MESSAGE);
				limpiarCharlie();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void consultarCharlie(String msj) {

		try {
			String nombreDepartamento = charlie.jtfNomDepartament.getText();
			nombreDepartamento = JOptionPane.showInputDialog(null, "Ingresa el departamento a " + msj);
			String instruccionBD = "SELECT * FROM departamento where nombreDepartamento='" + nombreDepartamento + "'";

			PreparedStatement ps = conect.getConexion().prepareStatement(instruccionBD);
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				charlie.jtfNomDepartament.setText(rs.getString(2));
			} else {
				JOptionPane.showMessageDialog(null, "El departamento no existe", "Alerta",
						JOptionPane.INFORMATION_MESSAGE);
			}
		} catch (SQLException ex) {
			JOptionPane.showMessageDialog(null, "error" + ex);
		}
	}

	public void modificarCharlie() {

		try {
			if (charlie.jbtModifier.getText().equals("Modificar")) {
				charlie.jbtModifier.setText("Confirmar");
				consultarCharlie("a modificar");

			} else {
				String instruccionDB = "UPDATE departamento SET nombreDepartamento='"
						+ WOEspacio(charlie.jtfNomDepartament.getText().trim()) + "'WHERE nombreDepartamento='"
						+ charlie.jtfNomDepartament.getText() + "'";
				PreparedStatement ps = conect.getConexion().prepareStatement(instruccionDB);
				ps.execute();
				charlie.jbtModifier.setText("Modificar");
				JOptionPane.showMessageDialog(null, "Registro modificado", "Query OK", JOptionPane.INFORMATION_MESSAGE);
				limpiarCharlie();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void eliminarCharlie() {

		try {
			if (charlie.jbtSupprimer.getText().equals("Eliminar")) {
				consultarCharlie("Eliminar");
				charlie.jbtSupprimer.setText("Confirmar");
			} else {
				String instruccionBD = "DELETE FROM departamento WHERE nombreDepartamento='"
						+ charlie.jtfNomDepartament.getText() + "'";
				PreparedStatement ps = conect.getConexion().prepareStatement(instruccionBD);
				ps.execute();
				charlie.jbtSupprimer.setText("Eliminar");
				limpiarCharlie();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void limpiarCharlie() {
		charlie.jtfNomDepartament.setText(null);
	}

	// DELTA PROFESSEUR

	public void llenarComboDelta() {
		System.out.println("sis");
		DefaultComboBoxModel<String> modelo = new DefaultComboBoxModel<String>();
		modelo.addElement("Elige:");
		try {

			String instruccionDB = "Select * from departamento";
			conect.Conectar();
			PreparedStatement ps = conect.getConexion().prepareStatement(instruccionDB);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				modelo.addElement(rs.getString(2));
			}
		} catch (SQLException ex) {
			JOptionPane.showMessageDialog(null, "MAL", ex.getMessage(), JOptionPane.INFORMATION_MESSAGE);

		}
		delta.jcbDepartament.setModel(modelo);
	}

	public int recuperarIdDelta(String nombre) {
		int recupera = 0;
		try {

			String instruccionDB = "SELECT id_departamento FROM departamento WHERE  nombreDepartamento = '" + nombre
					+ "'";
			PreparedStatement ps = conect.getConexion().prepareStatement(instruccionDB);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				recupera = rs.getInt(1);
			}
		} catch (SQLException e) {

		}
		System.out.println(recupera);
		return recupera;

	}

	public void insertarDelta() {

		if (delta.jcbDepartament.getSelectedItem().toString().equals("Elige:")) {
			JOptionPane.showMessageDialog(null, "Elija departamento", "Elegir", JOptionPane.INFORMATION_MESSAGE);
		} else {

			try {
				String instruccionDB = "INSERT INTO profesor VALUES(NULL,'" + delta.jtfPrenom.getText() + "','"
						+ delta.jtfNomPaternel.getText() + "','" + delta.jtfNomMaternel.getText() + "','"
						+ delta.jtfDateNaissance.getText() + "', '"
						+ recuperarIdDelta(delta.jcbDepartament.getSelectedItem().toString()) + "')";
				PreparedStatement ps = conect.getConexion().prepareStatement(instruccionDB);
				ps.execute();
				JOptionPane.showMessageDialog(null, "Registro agregado", "Query OK", JOptionPane.INFORMATION_MESSAGE);
				limpiarDelta();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

	}

	public void consultarDelta(String msj) {
		try {

			String nombre = delta.jtfPrenom.getText();
			nombre = JOptionPane.showInputDialog(null, "Ingresa el nombre a " + msj);

			String instruccionBD = "SELECT * FROM profesor WHERE nombre='" + nombre + "'";

			PreparedStatement ps = conect.getConexion().prepareStatement(instruccionBD);
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				delta.jtfPrenom.setText(rs.getString(2));
				delta.jtfNomPaternel.setText(rs.getString(3));
				delta.jtfNomMaternel.setText(rs.getString(4));
				delta.jtfDateNaissance.setText(rs.getString(5));
				delta.jcbDepartament.setSelectedIndex(rs.getInt(6));

			} else {
				JOptionPane.showMessageDialog(null, "El usuario no existe", "Alerta", JOptionPane.INFORMATION_MESSAGE);
			}

		} catch (SQLException ex) {
			JOptionPane.showMessageDialog(null, "error" + ex);
		}
	}

	public void modificarDelta() {

		try {
			String nombre = delta.jtfPrenom.getText();
			if (delta.jbtModifier.getText().equals("Modificar")) {
				delta.jbtModifier.setText("Confirmar");
				consultarDelta("a modificar");
			} else {
				String instruccionDB = "UPDATE profesor SET nombre='" + delta.jtfPrenom.getText() + "',aPaterno='"
						+ delta.jtfNomPaternel.getText() + "',aMaterno='" + delta.jtfNomMaternel.getText()
						+ "',fechaNacimiento='" + delta.jtfDateNaissance.getText() + "',id_departamento='"
						+ recuperarIdDelta(delta.jcbDepartament.getSelectedItem().toString()) + "'WHERE nombre='"
						+ nombre + "'";

				PreparedStatement ps = conect.getConexion().prepareStatement(instruccionDB);
				ps.execute();
				delta.jbtModifier.setText("Modificar");
				JOptionPane.showMessageDialog(null, "Registro modificado", "Query OK", JOptionPane.INFORMATION_MESSAGE);
				limpiarDelta();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void eliminarDelta() {

		try {
			if (delta.jbtSupprimer.getText().equals("Eliminar")) {
				consultarDelta("Eliminar");
				delta.jbtSupprimer.setText("Confirmar");
			} else {
				String instruccionBD = "DELETE FROM profesor WHERE nombre='" + nombre + "'";
				PreparedStatement ps = conect.getConexion().prepareStatement(instruccionBD);
				ps.execute();
				delta.jbtSupprimer.setText("Eliminar");
				limpiarDelta();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void limpiarDelta() {
		delta.jtfPrenom.setText(null);
		delta.jtfNomPaternel.setText(null);
		delta.jtfNomMaternel.setText(null);
		delta.jtfDateNaissance.setText(null);
		delta.jcbDepartament.setSelectedIndex(0);
	}

	// ECHO SUJET

	public void insertarEcho() {

		try {
			String asignatura = echo.jtfNomSujet.getText();
			asignatura = WOEspacio(echo.jtfNomSujet.getText().trim());

			if (asignatura.isEmpty()) {
				JOptionPane.showMessageDialog(null, "Existen campos vacios.", "Alerta",
						JOptionPane.INFORMATION_MESSAGE);
			} else {
				String instruccionDB = "INSERT INTO asignatura VALUES(NULL, '" + asignatura + "')";

				PreparedStatement ps = conect.getConexion().prepareStatement(instruccionDB);
				ps.execute();
				JOptionPane.showMessageDialog(null, "Registro agregado", "Query OK", JOptionPane.INFORMATION_MESSAGE);
				limpiarEcho();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void consultarEcho(String msj) {

		try {
			sujet.setNomSujet(JOptionPane.showInputDialog(null, "Ingresa la asignatura a " + msj));
			String instruccionBD = "SELECT * FROM asignatura where nombreAsignatura='" + sujet.getNomSujet() + "'";

			PreparedStatement ps = conect.getConexion().prepareStatement(instruccionBD);
			ResultSet rs = ps.executeQuery();
			try {
				if (rs.next()) {
					echo.jtfNomSujet.setText(rs.getString(2));
				} else {
					JOptionPane.showMessageDialog(null, "El asignatura no existe", "Alerta",
							JOptionPane.INFORMATION_MESSAGE);
				}
			} catch (SQLException ex) {
				JOptionPane.showMessageDialog(null, "error" + ex);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void modificarEcho() {

		try {
			if (echo.jbtModifier.getText().equals("Modificar")) {
				echo.jbtModifier.setText("Confirmar");
				consultarEcho("a modificar");
			} else {

				String instruccionDB = "UPDATE asignatura SET nombreAsignatura='"
						+ WOEspacio(echo.jtfNomSujet.getText().trim()) + "'WHERE nombreAsignatura='"
						+ echo.jtfNomSujet.getText() + "'";

				PreparedStatement ps = conect.getConexion().prepareStatement(instruccionDB);
				ps.execute();
				echo.jbtModifier.setText("Modificar");
				JOptionPane.showMessageDialog(null, "Registro modificado", "Query OK", JOptionPane.INFORMATION_MESSAGE);
				limpiarEcho();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void eliminarEcho() {

		try {
			if (echo.jbtSupprimer.getText().equals("Eliminar")) {
				consultarEcho("Eliminar");
				echo.jbtSupprimer.setText("Confirmar");
			} else {

				String instruccionBD = "DELETE FROM asignatura WHERE nombreAsignatura='" + echo.jtfNomSujet.getText()
						+ "'";
				PreparedStatement ps = conect.getConexion().prepareStatement(instruccionBD);
				ps.execute();
				echo.jbtSupprimer.setText("Eliminar");
				limpiarEcho();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void limpiarEcho() {
		echo.jtfNomSujet.setText(null);
	}

	// FOXTROT GROUPES

	public void llenarComboFoxtrot() {

		DefaultComboBoxModel<String> modelo = new DefaultComboBoxModel<String>();
		modelo.addElement("Elige:");
		try {

			String instruccionDB = "Select * from asignatura";
			PreparedStatement ps = conect.getConexion().prepareStatement(instruccionDB);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				modelo.addElement(rs.getString(2));
			}
		} catch (SQLException ex) {
			JOptionPane.showMessageDialog(null, "MAL", ex.getMessage(), JOptionPane.INFORMATION_MESSAGE);
		}
		foxtrot.jcbSujet.setModel(modelo);
	}

	public int recuperarIdFoxtrot(String nombre) {
		int recupera = 0;
		try {
			String instruccionDB = "SELECT id_asignatura FROM asignatura WHERE  nombreAsignatura = '" + nombre + "'";
			PreparedStatement ps = conect.getConexion().prepareStatement(instruccionDB);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				recupera = rs.getInt(1);
			}

		} catch (SQLException e) {

		}
		return recupera;
	}

	public void limpiarFoxtrot() {
		foxtrot.jcbSujet.setSelectedIndex(0);
	}

	// GOLF CLASSE

	public void insertarGolf() {

		try {
			WOEspacio(golf.jtfClasse.getText().trim());

			if (golf.jtfClasse.getText().isEmpty()) {
				JOptionPane.showMessageDialog(null, "Existen campos vacios.", "Alerta",
						JOptionPane.INFORMATION_MESSAGE);
			} else {
				String instruccionDB = "INSERT INTO aula VALUES(NULL, '" + golf.jtfClasse.getText() + "')";
				PreparedStatement ps = conect.getConexion().prepareStatement(instruccionDB);
				ps.execute();
				JOptionPane.showMessageDialog(null, "Registro agregado", "Query OK", JOptionPane.INFORMATION_MESSAGE);
				limpiarGolf();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void consultarGolf(String msj) {
		try {
			classe.setTypeClasse(JOptionPane.showInputDialog(null, "Ingresa el aula a " + msj));
			String instruccionBD = "SELECT * FROM aula where tipo_aula='" + golf.jtfClasse.getText() + "'";

			PreparedStatement ps = conect.getConexion().prepareStatement(instruccionBD);
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				golf.jtfClasse.setText(rs.getString(2));
			} else {
				JOptionPane.showMessageDialog(null, "El aula no existe", "Alerta", JOptionPane.INFORMATION_MESSAGE);
			}
		} catch (SQLException ex) {
			JOptionPane.showMessageDialog(null, "error" + ex);
		}
	}

	public void modificarGolf() {

		try {
			if (golf.jbtModifier.getText().equals("Modificar")) {
				golf.jbtModifier.setText("Confirmar");
				consultarGolf("a modificar");

			} else {
				String instruccionDB = "UPDATE aula SET tipo_aula='" + WOEspacio(golf.jtfClasse.getText().trim())
						+ "'WHERE tipo_aula='" + golf.jtfClasse.getText() + "'";

				PreparedStatement ps = conect.getConexion().prepareStatement(instruccionDB);
				ps.execute();
				golf.jbtModifier.setText("Modificar");
				JOptionPane.showMessageDialog(null, "Registro modificado", "Query OK", JOptionPane.INFORMATION_MESSAGE);
				limpiarGolf();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void eliminarGolf() {

		try {
			if (golf.jbtSupprimer.getText().equals("Eliminar")) {
				consultarGolf("Eliminar");
				golf.jbtSupprimer.setText("Confirmar");
			} else {
				String instruccionBD = "DELETE FROM aula WHERE tipo_aula='" + golf.jtfClasse.getText() + "'";
				PreparedStatement ps = conect.getConexion().prepareStatement(instruccionBD);
				ps.execute();
				golf.jbtSupprimer.setText("Eliminar");
				limpiarGolf();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public void limpiarGolf() {
		golf.jtfClasse.setText(null);
	}

}
