package main;

import controleur.Control;
import modeles.Classe;
import modeles.Departament;
import modeles.Etudiant;
import modeles.Groupe;
import modeles.Professeur;
import modeles.Sujet;
import vues.Alpha;

public class Main {

	public static void main(String[] args) {

		Alpha alpha = new Alpha();
		Classe classe = new Classe();
		Departament departament = new Departament();
		Etudiant etudiant = new Etudiant();
		Groupe groupe = new Groupe();
		Professeur professeur = new Professeur();
		Sujet sujet = new Sujet();
		
		Control control = new Control(alpha, classe, departament, etudiant, groupe, professeur, sujet);
alpha.setVisible(true);
		
	}

}
