package modeles;

public class Sujet {

	private int idSujet;
	private String nomSujet;
	
	public Sujet() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Sujet(int idSujet, String nomSujet) {
		super();
		this.idSujet = idSujet;
		this.nomSujet = nomSujet;
	}

	public int getIdSujet() {
		return idSujet;
	}

	public void setIdSujet(int idSujet) {
		this.idSujet = idSujet;
	}

	public String getNomSujet() {
		return nomSujet;
	}

	public void setNomSujet(String nomSujet) {
		this.nomSujet = nomSujet;
	}
}
