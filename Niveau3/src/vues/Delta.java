package vues;

import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * 
 * @author pearbit-zero
 *
 */


public class Delta extends JFrame {

	public JPanel contentPane;
	public JTextField jtfDateNaissance;
	public JTextField jtfNomMaternel;
	public JTextField jtfNomPaternel;
	public JTextField jtfPrenom;
	public JComboBox jcbDepartament;
	public String nombre;
	public JButton jbtModifier;
	public JButton jbtSupprimer;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Delta frame = new Delta();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Delta() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 503, 256);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel jpPanel = new JPanel();
		contentPane.add(jpPanel, BorderLayout.CENTER);
		jpPanel.setLayout(null);

		JLabel jlbTitre = new JLabel("Enregistrer du professeurs");
		jlbTitre.setBounds(126, 26, 196, 15);
		jpPanel.add(jlbTitre);

		JLabel jlbPrenom = new JLabel("Prenom:");
		jlbPrenom.setBounds(12, 55, 70, 15);
		jpPanel.add(jlbPrenom);

		JLabel jlbNomPaternel = new JLabel("Nom paternel:");
		jlbNomPaternel.setBounds(12, 91, 143, 15);
		jpPanel.add(jlbNomPaternel);

		JLabel jlbNomMaternel = new JLabel("Nom maternel:");
		jlbNomMaternel.setBounds(12, 118, 143, 15);
		jpPanel.add(jlbNomMaternel);

		JLabel jlbDateNaissance = new JLabel("Date de naissance:");
		jlbDateNaissance.setBounds(12, 145, 156, 15);
		jpPanel.add(jlbDateNaissance);

		JLabel jlbDepartament = new JLabel("Departament:");
		jlbDepartament.setBounds(12, 172, 177, 15);
		jpPanel.add(jlbDepartament);

		jcbDepartament = new JComboBox();
		jcbDepartament.setBounds(184, 167, 112, 24);
		jpPanel.add(jcbDepartament);

		jtfDateNaissance = new JTextField();
		jtfDateNaissance.setBounds(182, 143, 114, 19);
		jpPanel.add(jtfDateNaissance);
		jtfDateNaissance.setColumns(10);

		jtfNomMaternel = new JTextField();
		jtfNomMaternel.setBounds(182, 116, 114, 19);
		jpPanel.add(jtfNomMaternel);
		jtfNomMaternel.setColumns(10);

		jtfNomPaternel = new JTextField();
		jtfNomPaternel.setBounds(182, 85, 114, 19);
		jpPanel.add(jtfNomPaternel);
		jtfNomPaternel.setColumns(10);

		jtfPrenom = new JTextField();
		jtfPrenom.setBounds(182, 53, 114, 19);
		jpPanel.add(jtfPrenom);
		jtfPrenom.setColumns(10);

		JButton jbtSauvegarder = new JButton("Sauvegarder");
		jbtSauvegarder.setBounds(339, 50, 142, 25);
		jpPanel.add(jbtSauvegarder);
		
		jbtModifier = new JButton("Modifier");
		jbtModifier.setBounds(339, 86, 142, 25);
		jpPanel.add(jbtModifier);
		
		jbtSupprimer = new JButton("Supprimer");
		jbtSupprimer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		jbtSupprimer.setBounds(339, 118, 142, 25);
		jpPanel.add(jbtSupprimer);
		
		JButton jbtRetourner = new JButton("Retourner");
		jbtRetourner.setBounds(339, 162, 142, 25);
		jpPanel.add(jbtRetourner);
	}
}
