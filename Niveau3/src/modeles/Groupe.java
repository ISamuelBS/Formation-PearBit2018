package modeles;

public class Groupe {

	private int idGroupe;

	public Groupe() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Groupe(int idGroupe) {
		super();
		this.idGroupe = idGroupe;
	}

	public int getIdGroupe() {
		return idGroupe;
	}

	public void setIdGroupe(int idGroupe) {
		this.idGroupe = idGroupe;
	}
}
