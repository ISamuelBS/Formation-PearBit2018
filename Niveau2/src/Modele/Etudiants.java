package Modele;

import java.util.ArrayList;

public class Etudiants extends Personnes{

	private String matricule;
	private String generation;
	private String horaire;
	private ArrayList<String> etudiants = new ArrayList<String>();
	
	
	public Etudiants() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Etudiants(String matricule, String generation, String horaire, ArrayList<String> etudiants) {
		super();
		this.matricule = matricule;
		this.generation = generation;
		this.horaire = horaire;
		this.etudiants = etudiants;
	}

	public ArrayList<String> getEtudiants() {
		return etudiants;
	}

	public void setEtudiants(ArrayList<String> etudiants) {
		this.etudiants = etudiants;
	}

	public String getMatricule() {
		return matricule;
	}

	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}

	public String getGeneration() {
		return generation;
	}

	public void setGeneration(String generation) {
		this.generation = generation;
	}

	public String getHoraire() {
		return horaire;
	}

	public void setHoraire(String horaire) {
		this.horaire = horaire;
	}
}
