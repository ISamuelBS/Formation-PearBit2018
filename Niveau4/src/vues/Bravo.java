package vues;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;

/**
 * 
 * @author pearbit-zero
 *
 */

public class Bravo extends JFrame {

	public JPanel contentPane;
	public JTextField jtfTelephonique;
	public JTextField jtfNumero;
	public JTextField jtfCodePays;
	public JButton jbtSauvegarder;
	public JButton jbtSuivant;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Bravo frame = new Bravo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Bravo() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 304, 250);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel jpPanel = new JPanel();
		contentPane.add(jpPanel, BorderLayout.CENTER);
		jpPanel.setLayout(null);
		
		JLabel jlbTitre = new JLabel("Enregistrer telephones");
		jlbTitre.setFont(new Font("Dialog", Font.BOLD, 12));
		jlbTitre.setBounds(48, 12, 217, 31);
		jpPanel.add(jlbTitre);
		
		JLabel jlbNumero = new JLabel("Numéro:");
		jlbNumero.setBounds(12, 44, 115, 15);
		jpPanel.add(jlbNumero);
		
		JLabel jlbCodePays = new JLabel("Code du pays:");
		jlbCodePays.setBounds(12, 71, 115, 15);
		jpPanel.add(jlbCodePays);
		
		JLabel jlbTelephonique = new JLabel("Téléphonique:");
		jlbTelephonique.setBounds(12, 98, 115, 15);
		jpPanel.add(jlbTelephonique);
		
		jtfNumero = new JTextField();
		jtfNumero.setBounds(128, 41, 137, 19);
		jpPanel.add(jtfNumero);
		jtfNumero.setColumns(10);
		
		jtfCodePays = new JTextField();
		jtfCodePays.setBounds(128, 69, 137, 19);
		jpPanel.add(jtfCodePays);
		jtfCodePays.setColumns(10);
		
		jtfTelephonique = new JTextField();
		jtfTelephonique.setBounds(128, 96, 137, 19);
		jpPanel.add(jtfTelephonique);
		jtfTelephonique.setColumns(10);
		
		jbtSauvegarder = new JButton("Sauvegarder");
		jbtSauvegarder.setBounds(12, 131, 132, 25);
		jpPanel.add(jbtSauvegarder);
		
		jbtSuivant = new JButton("Suivante");
		jbtSuivant.setBounds(141, 131, 117, 25);
		jpPanel.add(jbtSuivant);
	}
}