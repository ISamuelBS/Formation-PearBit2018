package vues;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

/**
 * 
 * @author pearbit-zero
 *
 */

public class Bravo extends JFrame {

	public JPanel contentPane;
	public JTextField jtfMatricule;
	public JTextField jtfPrenom;
	public JTextField jtfNomPaternel;
	public JTextField jtfNomMaternel;
    public JTextField jtfDateNaissance;
    public JButton jbtModifier;
    public JButton jbtSupprimer;
        

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Bravo frame = new Bravo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Bravo() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 299);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel jpPanel = new JPanel();
		contentPane.add(jpPanel, BorderLayout.CENTER);
		jpPanel.setLayout(null);
		
		JLabel jlbTitre = new JLabel("Enregistrer étudiants");
		jlbTitre.setBounds(148, 22, 175, 14);
		jpPanel.add(jlbTitre);
		
		JLabel jlbMatricule = new JLabel("Matricule:");
		jlbMatricule.setBounds(10, 51, 84, 14);
		jpPanel.add(jlbMatricule);
		
		JLabel jlbPrenom = new JLabel("Prenom:");
		jlbPrenom.setBounds(10, 81, 84, 14);
		jpPanel.add(jlbPrenom);
		
		JLabel jlbNomPaternel = new JLabel("Nom paternel:");
		jlbNomPaternel.setBounds(10, 112, 126, 14);
		jpPanel.add(jlbNomPaternel);
		
		JLabel jlbNomMaternel = new JLabel("Nom maternel:");
		jlbNomMaternel.setBounds(10, 140, 126, 14);
		jpPanel.add(jlbNomMaternel);
		
		JLabel jlbDateNaissance = new JLabel("Date de naissance:");
		jlbDateNaissance.setBounds(10, 171, 175, 14);
		jpPanel.add(jlbDateNaissance);
		
		jtfMatricule = new JTextField();
		jtfMatricule.setBounds(168, 48, 86, 20);
		jpPanel.add(jtfMatricule);
		jtfMatricule.setColumns(10);
		
		jtfPrenom = new JTextField();
		jtfPrenom.setBounds(168, 79, 86, 20);
		jpPanel.add(jtfPrenom);
		jtfPrenom.setColumns(10);
		
		jtfNomPaternel = new JTextField();
		jtfNomPaternel.setBounds(168, 110, 86, 20);
		jpPanel.add(jtfNomPaternel);
		jtfNomPaternel.setColumns(10);
		
		jtfNomMaternel = new JTextField();
		jtfNomMaternel.setBounds(168, 138, 86, 20);
		jpPanel.add(jtfNomMaternel);
		jtfNomMaternel.setColumns(10);
		
		jtfDateNaissance = new JTextField();
		jtfDateNaissance.setBounds(168, 169, 86, 20);
		jpPanel.add(jtfDateNaissance);
		jtfDateNaissance.setColumns(10);
		
		JButton jbtChercher = new JButton("Chercher");
		
		JButton jbtSauvegarder = new JButton("Sauvegarder");

		jbtSauvegarder.setBounds(286, 151, 126, 25);
		jpPanel.add(jbtSauvegarder);
		jbtChercher.setBounds(286, 48, 126, 23);
		jpPanel.add(jbtChercher);
		
		jbtModifier = new JButton("Modifier");
		jbtModifier.setBounds(286, 81, 126, 23);
		jpPanel.add(jbtModifier);
		
		jbtSupprimer = new JButton("Supprimer");
		jbtSupprimer.setBounds(286, 116, 126, 23);
		jpPanel.add(jbtSupprimer);
		
		JButton jbtRetourner = new JButton("Retourner");
		jbtRetourner.setBounds(286, 188, 126, 25);
		jpPanel.add(jbtRetourner);
	}
}
