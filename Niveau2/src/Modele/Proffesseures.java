package Modele;

import java.util.ArrayList;

import Modele.Personnes;

public class Proffesseures extends Personnes{

	private String cedule;
	private String specialite;
	private int nombreCabine;
	ArrayList<String> professeurs = new ArrayList<String>();
	
	public Proffesseures() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Proffesseures(String cedule, String specialite, int nombreCabine, ArrayList<String> professeurs) {
		super();
		this.cedule = cedule;
		this.specialite = specialite;
		this.nombreCabine = nombreCabine;
		this.professeurs = professeurs;
	}

	public String getCedule() {
		return cedule;
	}

	public void setCedule(String cedule) {
		this.cedule = cedule;
	}

	public String getSpecialite() {
		return specialite;
	}

	public void setSpecialite(String specialite) {
		this.specialite = specialite;
	}

	public int getNombreCabine() {
		return nombreCabine;
	}

	public void setNombreCabine(int nombreCabine) {
		this.nombreCabine = nombreCabine;
	}

	public ArrayList<String> getProfesseurs() {
		return professeurs;
	}

	public void setProfesseurs(ArrayList<String> professeurs) {
		this.professeurs = professeurs;
	}	
}
