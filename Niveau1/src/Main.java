import Model.Model;
import View.Alpha;
import View.Bravo;

import java.util.Arrays;

import Controler.Control;

public class Main {

	/**
	 * Méthode main de niveu 1
	 * Cette méthode est le principale pour exécuter l'aplications
	 * @param args
	 */
	
	public static void main(String args[]) {
		Alpha view = new Alpha();
		Bravo view2 = new Bravo();
		Model model = new Model();
		Control control = new Control(model, view, view2);
		view.setVisible(true);
	}
}