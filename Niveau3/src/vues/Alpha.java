package vues;

import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;

/**
 * 
 * @author pearbit-zero
 *
 */

public class Alpha extends JFrame {

	public JPanel contentPane;
	public JButton jbtEtudiant;
	public JLabel lblMenu;
	public JButton jbtSujet;
	public JButton jbtClasse;
	public JButton jbtDepartament;
	public JButton jbtGroupe;
	public JButton jbtProfesseur;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Alpha frame = new Alpha();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Alpha() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 420);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel jpPanel = new JPanel();
		contentPane.add(jpPanel, BorderLayout.CENTER);
		jpPanel.setLayout(null);

		jbtEtudiant = new JButton("Étudiants");
		jbtEtudiant.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		jbtEtudiant.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				Bravo obj = new Bravo();
				obj.setVisible(true);
				ocultar();
			}
		});
		
				lblMenu = new JLabel("MENU");
				lblMenu.setFont(new Font("Dialog", Font.BOLD, 20));
				lblMenu.setBounds(145, 40, 88, 40);
				jpPanel.add(lblMenu);
		jbtEtudiant.setBounds(103, 103, 167, 25);
		jpPanel.add(jbtEtudiant);

		jbtSujet = new JButton("Sujet");
		jbtSujet.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				Echo obj = new Echo();
				obj.setVisible(true);
				ocultar();
			}
		});
		jbtSujet.setBounds(103, 140, 167, 25);
		jpPanel.add(jbtSujet);

		jbtClasse = new JButton("Classe");

		jbtClasse.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				Golf obj = new Golf();
				obj.setVisible(true);
				ocultar();
			}
		});
		jbtClasse.setBounds(103, 177, 167, 25);
		jpPanel.add(jbtClasse);

		jbtDepartament = new JButton("Departments");
		jbtDepartament.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				Charlie obj = new Charlie();
				obj.setVisible(true);
				ocultar();
			}
		});
		jbtDepartament.setBounds(103, 214, 167, 25);
		jpPanel.add(jbtDepartament);

		jbtGroupe = new JButton("Groupes");
		jbtGroupe.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				Foxtrot obj = new Foxtrot();
				obj.setVisible(true);
				ocultar();
			}
		});
		jbtGroupe.setBounds(103, 251, 167, 25);
		jpPanel.add(jbtGroupe);

		jbtProfesseur = new JButton("Professeur");
		jbtProfesseur.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {

				Delta obj = new Delta();
				obj.setVisible(true);
				ocultar();
			}
		});
		jbtProfesseur.setBounds(103, 288, 167, 25);
		jpPanel.add(jbtProfesseur);
	}

	private void ocultar() {
		this.dispose();
	}

}
