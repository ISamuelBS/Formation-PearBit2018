package modeles;

public class Classe {

	private int idClasse;
	private String typeClasse;
	
	public Classe() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Classe(int idClasse, String typeClasse) {
		super();
		this.idClasse = idClasse;
		this.typeClasse = typeClasse;
	}

	public int getIdClasse() {
		return idClasse;
	}

	public void setIdClasse(int idClasse) {
		this.idClasse = idClasse;
	}

	public String getTypeClasse() {
		return typeClasse;
	}

	public void setTypeClasse(String typeClasse) {
		this.typeClasse = typeClasse;
	}
}
