package vues;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

/**
 * 
 * @author pearbit-zero
 *
 */

public class Golf extends JFrame {

	public JPanel contentPane;
	public JTextField jtfClasse;
	public JButton jbtSauvegarder;
	public JButton jbtModifier;
	public JButton jbtSupprimer;
	public JButton jbtRetourner;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Golf frame = new Golf();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Golf() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 513, 212);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel jpPanel = new JPanel();
		contentPane.add(jpPanel, BorderLayout.CENTER);
		jpPanel.setLayout(null);

		JLabel jlbTitre = new JLabel("Enregistrer classes");
		jlbTitre.setBounds(139, 12, 146, 15);
		jpPanel.add(jlbTitre);

		JLabel jlbClasse = new JLabel("Type de la classe:");
		jlbClasse.setBounds(0, 57, 178, 15);
		jpPanel.add(jlbClasse);

		jtfClasse = new JTextField();
		jtfClasse.setBounds(128, 55, 146, 19);
		jpPanel.add(jtfClasse);
		jtfClasse.setColumns(10);

		jbtSauvegarder = new JButton("Sauvergarder");
		jbtSauvegarder.setBounds(304, 12, 130, 25);
		jpPanel.add(jbtSauvegarder);

		jbtModifier = new JButton("Modifier");
		jbtModifier.setBounds(304, 49, 130, 25);
		jpPanel.add(jbtModifier);

		jbtSupprimer = new JButton("Supprimer");
		jbtSupprimer.setBounds(304, 86, 130, 25);
		jpPanel.add(jbtSupprimer);

		jbtRetourner = new JButton("Retourner");
		jbtRetourner.setBounds(304, 123, 130, 25);
		jpPanel.add(jbtRetourner);
	}
}
