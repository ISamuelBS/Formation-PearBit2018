package modeles;

public class Professeur extends Personne{

	private int idProfesseur;
	
	public Professeur() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Professeur(String prenom, String nomPaternel, String nomMaternel, String dateNaissance) {
		super(prenom, nomPaternel, nomMaternel, dateNaissance);
		// TODO Auto-generated constructor stub
	}

	public int getIdProfesseur() {
		return idProfesseur;
	}

	public void setIdProfesseur(int idProfesseur) {
		this.idProfesseur = idProfesseur;
	}
	
	
	
}
